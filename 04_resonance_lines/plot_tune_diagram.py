#%%
import beamtools.resonance_lines as brl
import matplotlib.pyplot as plt

#%%
orders = [1,2,3,4,5,6,7,8]
tune_diagram=brl.resonance_lines([3.95,4.52],[3.95,4.52], orders, 16)
fig,ax = plt.subplots(1,1, figsize=(6,6), facecolor='white')
fontsize=20
fig=tune_diagram.plot_resonance(figure_object=fig)
ax.set_xlabel('$\mathrm{Q_x}$', fontsize=fontsize)
ax.set_ylabel('$\mathrm{Q_y}$', fontsize=fontsize)
ax.tick_params(axis='both', labelsize=fontsize)
ax.set_title('Tune Diagram', fontsize=fontsize)
ax.plot([4.40],[4.45],'*',ms=15,color='black')
fig.tight_layout()
fig.savefig('tune_diagram_order_%s.png'%orders[-1], dpi=300)

# %%
