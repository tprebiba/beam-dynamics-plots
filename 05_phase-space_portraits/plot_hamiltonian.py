#%%
import numpy as np
import matplotlib.pyplot as plt

def hamiltonian(R,psi,Delta,Omega,n):
    # Delta: detuning parameter
    # Omega: tune spread parameter
    # n: resonance order
    return R*Delta + R**2*Omega + R**(n/2)*np.cos(n*psi)

#%%
num_points = 250
#R = np.random.uniform(low=0.1, high=1.0, size=num_points)
#psi = np.random.uniform(low=0, high=2*np.pi, size=num_points)
R = np.linspace(0.,1.5,num_points)
psi = np.linspace(0,2*np.pi,num_points)
R, psi = np.meshgrid(R,psi)
X, Y = R*np.cos(psi), R*np.sin(psi)

Delta = -3
Omega = 0
n = 4 
H = hamiltonian(R,psi,Delta,Omega,n)

f,ax = plt.subplots(1,1, figsize=(5,4), facecolor='white')
fontsize=12
#ax.set_xlabel(r'$x$', fontsize=fontsize)
#ax.set_ylabel(r'$p_x$', fontsize=fontsize)
ax.set_xlim(-1,1)
ax.set_ylim(-1,1)
#ax.tick_params(axis='both', labelsize=fontsize)
ax.tick_params(axis='both',left=False, bottom=False, labelleft=False, labelbottom=False)
ax.set_title(r'$\Delta = $'+str(Delta)+r', $\Omega = $'+str(Omega)+r', $n = $'+str(n), fontsize=fontsize)
cp = ax.contour(X,Y,H,levels=40, colors='black',linestyles='solid', linewidths=0.5)
f.tight_layout()
#f.savefig('hamiltonian_Delta'+str(Delta)+'_Omega'+str(Omega)+'_n'+str(n)+'.png', dpi=300)
plt.show()

# %%
