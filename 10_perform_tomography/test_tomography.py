#%%
import numpy as np
import matplotlib.pyplot as plt
import json
from skimage.transform import radon, iradon


with open('angles_for_tomography.json') as json_file:
    data = json.load(json_file)


# %%
f,ax = plt.subplots(1,1, figsize=(5,4), facecolor='white')
fontsize=20
ax.set_xlabel(r'$y$ [mm]', fontsize=fontsize)
ax.set_ylabel(r'Profile density [a.u.]', fontsize=fontsize)
ax.tick_params(axis='both', labelsize=fontsize)
#ax.tick_params(axis='y', left=False, labelleft=False)
angles = np.array(data['_angles_for_tomography'])
every = 16
positions = []
profiles = []
#indx0 = 695
indx0 = 500
#indx0 = 800
indx1 = 2060
for i in range(len(angles)):
    profile = np.array(data['_profiles_for_tomography'][i][indx0:indx1][0::every])/1e5
    profile_min = np.min(profile)
    profile -= profile_min
    position = np.array(data['_positions_for_tomography'][i][indx0:indx1][0::every])
    profile += np.abs(position)*profile/2 # to enhance the islands...
    profiles.append(profile)
    positions.append(position)
    ax.plot(positions[i],profiles[i]+i/50*0, lw=0.5, c='black')
    ax.axvline(position[len(position)//2]) # iradon assumes that rotational axis is around the middle of the image
angles, indices = np.unique(angles, return_index=True) # remove duplicates
profiles  = np.vstack(profiles)
profiles = profiles[indices] # remove duplicates

#ax.set_xlim(-19,19)

#%%
# Tomography
_angles = angles
_image = profiles.transpose()
#sinogram = radon(_image, theta=_angles, circle=False)
reconstructed_image = iradon(_image, theta=_angles, 
                             circle=False, filter_name='hann', interpolation='cubic',preserve_range=True
                             )
    
# Display the original and reconstructed images
fig, axes = plt.subplots(1, 2, figsize=(12, 4),gridspec_kw={'width_ratios': [1, 1]})
fontsize=15
axes[0].imshow(_image, cmap='gray')
axes[0].set_title('Sinogram', fontsize=fontsize)
im1 = axes[1].imshow(reconstructed_image, cmap='gray')
cbar = fig.colorbar(im1, ax=axes.ravel().tolist(), shrink=1)
cbar.set_label('Amplitude [a.u.]', fontsize=fontsize)
cbar.ax.tick_params(length=0, labelsize=0)
axes[1].set_title('Reconstructed 2D image', fontsize=fontsize)
#cbar.set_label('Intensity')
plt.show()
fig.savefig('tomography.png', dpi=300)
# %%
