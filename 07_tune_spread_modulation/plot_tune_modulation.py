#%%
import numpy as np
import matplotlib.pyplot as plt
from polygons import p1,p2,p3,p4
import matplotlib.patches as mpatches
import matplotlib.collections as mcollections
import matplotlib.colors as mcolors
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.cm as cm




#%%
# Beam edge
x = np.linspace(-0.5, 0.5, 100)  # Generate 100 x-values between -0.5 and 0.5
y = -(4 * x ** 2 - 1)  # Calculate the corresponding y-values for the parabolic function
# Beam particles
np.random.seed(42)
num_particles = 1000
#particle_x = np.random.uniform(-0.5, 0.5, num_particles)
#particle_y = np.random.uniform(-1, 1, num_particles)
particle_x = np.random.normal(0, 0.22, num_particles)
particle_y = np.random.normal(0, 0.35, num_particles)
mask = np.where((particle_y < -(4 * particle_x ** 2 - 1)) & (particle_y > (4 * particle_x ** 2 - 1)))
particle_x = particle_x[mask]
particle_y = particle_y[mask]



#%%
# Create the figure and axis
fig, axs = plt.subplots(1,2,figsize=(10,4),gridspec_kw={'width_ratios': [2, 1]})
fontsize=20
ax = axs[0]
ax.set_xlabel(r'$s$ (m)',fontsize=fontsize)
ax.set_ylabel(r'$y$ [mm]',fontsize=fontsize)
ax.tick_params(labelsize=fontsize-5)
ax.axhline(0.0,linestyle='-',color='black',lw=1)
ax.tick_params(axis='both',labelleft=False,left=False,bottom=False,labelbottom=False)
ax.plot(x,y,lw=2,color='black')
ax.plot(x,-y,lw=2,color='black')
ax.set_xlim(-0.7,0.7)
ax.set_ylim(-1.5,1.5)
ax.scatter(particle_x, particle_y, marker='.',  s=15,color='black',alpha=0.15)  # Plot the beam particles as circles

ax = axs[1]
#ax.set_aspect('equal')
#ax.autoscale_view()
ax.set_xlim(4.16, 4.34)
ax.set_ylim(4.36, 4.59)
ax.set_ylabel(r'$Q_y$',fontsize=fontsize)
ax.set_xlabel(r'$Q_x$',fontsize=fontsize)
ax.tick_params(labelsize=fontsize-5)


# Set the desired position for the maximum point
desired_max_x = 4.3
desired_max_y = 4.55

#for scaling_factor,colors,s0,c0 in zip([1.5],['Blues'],[0],['blue']):
for scaling_factor,colors,s0,c0 in zip([1.5,1.0],['Blues','Reds'],[0,0.18],['blue','red']):
#for scaling_factor,colors,s0,c0 in zip([1.5,1.0,0.5],['Blues','Reds','Greens'],[0,0.18,0.38],['blue','red','green']):
#for scaling_factor,colors in zip([1.5,1.3,1.1,0.9,0.7,0.5],['Blues','Blues','Blues','Blues','Blues','Blues']):
    

    ###########################################
    # Add the rectangular in the line density
    ###########################################
    ax = axs[0]
    line1_x = s0 - 0.01
    line2_x = s0 + 0.01
    nr_rect = 282
    line_y = np.linspace(-y[np.abs(x - s0).argmin()], y[np.abs(x - s0).argmin()], nr_rect)
    alphas = np.linspace(0,0.7, len(line_y[0::2]))
    alphas = np.concatenate([alphas, alphas[::-1]])
    for i in range(len(line_y)-1):
        ax.fill_betweenx(line_y[i:i+1], line1_x, line2_x,facecolor='none',edgecolor=c0,alpha=alphas[i])
    
    
    ###########################################
    # Add the rectangular in the line density
    ###########################################
    ax = axs[1]
    # Polygon scaling
    #scaling_factor = 1.5
    # Define polygons
    Polygons = np.transpose(np.stack((p1, p2, p3, p4)), (1, 0, 2)) * scaling_factor
    # Calculate the maximum x and y values
    max_x = np.max(Polygons[:, :, 0])
    max_y = np.max(Polygons[:, :, 1])
    # Calculate the translation values for the maximum point
    translate_x = desired_max_x - max_x
    translate_y = desired_max_y - max_y
    # Translate the polygons to adjust the position of the maximum point
    Polygons[:, :, 0] += translate_x
    Polygons[:, :, 1] += translate_y
    # Create the PatchCollection object
    patches = [mpatches.Polygon(poly) for poly in Polygons]
    # Get the minimum and maximum values of x and y axes
    xmin, xmax = np.min(Polygons[:, :, 0]), np.max(Polygons[:, :, 0])
    ymin, ymax = np.min(Polygons[:, :, 1]), np.max(Polygons[:, :, 1])
    # Calculate alpha values based on the relative positions of polygons
    alpha_values = 1.0 - ((Polygons[:, 0, 0] - xmin) / (xmax - xmin) + (Polygons[:, 0, 1] - ymin) / (ymax - ymin))
    alpha_values = np.clip(alpha_values, 0.0, 1)  # Force values to be between 0 and 1
    # Set the colormap and create the color array with fading alpha values
    cmap = plt.cm.get_cmap(colors)
    p_collection = mcollections.PatchCollection(patches, edgecolor='k', linewidth=0.5)
    p_collection.set_array(np.array(alpha_values))
    p_collection.set_cmap(cmap)
    # Function to toggle visibility of connecting lines
    def toggle_connecting_lines():
        p_collection.set_linewidth(0.0 if p_collection.get_linewidth() > 0.0 else 0.5)
        plt.draw()
    toggle_connecting_lines()
    ax.add_collection(p_collection)
    # Add a colorbar to show the fading effect
    #cbar = plt.colorbar(p_collection)
    #cbar.set_label('Alpha')


fig.tight_layout()
plt.show()
#fig.savefig('tune_spread_line_density_1.png', dpi=400)
fig.savefig('tune_spread_line_density_2.png', dpi=400)
#fig.savefig('tune_spread_line_density_3.png', dpi=400)

# %%
