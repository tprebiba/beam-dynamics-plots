#%%
import xtrack as xt
import xpart as xp
import xobjects as xo
import xfields as xf

import numpy as np
import matplotlib.pyplot as plt
import json




#%%
#############################
# Get PSB line and particles
#############################
line = xt.Line.from_json('psb_line_4p17_4p23_madthin10_at_tstr1l1.json')
line.build_tracker()
n_part = int(1.5e3)
bunch_intensity = 40e10
emitx = 1.0e-6
emity = 1.0e-6
sigma_z = (271/4)*0.525*0.3
particles = xp.generate_matched_gaussian_bunch(num_particles=int(n_part), total_intensity_particles=bunch_intensity, 
                                                nemitt_x=emitx, nemitt_y=emity, sigma_z=sigma_z,
                                                line=line)
with open(f'outputs/particles_ini.json', 'w') as fid:
      json.dump(particles.to_dict(), fid, cls=xo.JEncoder)





#%%
#############################
# Get PSB line and particles
#############################
num_turns = 1
for i in range(num_turns):
    line.track(particles, num_turns=1, 
            #turn_by_turn_monitor=True,
            turn_by_turn_monitor='ONE_TURN_EBE'
                )
    np.save(f'outputs/s{i}', line.record_last_track.s)
    np.save(f'outputs/x{i}',line.record_last_track.x)
    np.save(f'outputs/px{i}',line.record_last_track.px)
    np.save(f'outputs/y{i}',line.record_last_track.y)
    np.save(f'outputs/py{i}',line.record_last_track.py)
    np.save(f'outputs/z{i}',line.record_last_track.zeta)
    np.save(f'outputs/dp{i}',line.record_last_track.delta)


# %%
