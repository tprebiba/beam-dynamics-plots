#%%
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import optics.psb_lattice as psb
from cpymad.madx import Madx
import matplotlib.patches as patches
from mpl_toolkits.mplot3d import Axes3D


#%%
#----------------------------------------------------------------------
# Get data
#----------------------------------------------------------------------
s0 = np.load('../outputs/s0.npy')
y0 = np.load('../outputs/y0.npy')
x0 = np.load('../outputs/x0.npy')
z0 = np.load('../outputs/z0.npy')
py0 = np.load('../outputs/py0.npy')
n_part = len(y0)
print('particles', len(y0))
print('elements', len(y0[0]))
# %%


# %%
#----------------------------------------------------------------------
# Phase trajectories of particles in y-s
#----------------------------------------------------------------------
f,ax = plt.subplots(1,1,figsize=(15,4), facecolor='white')
fontsize=20
ax.set_xlabel('$s$ (m)', fontsize=fontsize)
ax.set_ylabel('$y$ (mm)', fontsize=fontsize)
ax.tick_params(labelsize=fontsize)
# PSB periodicity
for i in range(17):
    ax.axvline(157.08/16*i, ls='--', lw=1, c='grey')
# All particles
#for i in range(n_part):
#    ax.plot(s0[i,:], y0[i,:]*1e3,'-',ms=1.5,c='blue', lw=0.05)
# Single particle
single_particle_id = 0
ax.plot(s0[single_particle_id,:], y0[single_particle_id,:]*1e3,'-',ms=1.5,c='red', lw=5)
ax.set_ylim(-21,21)
ax.set_yticks([-20, 0, 20])
ax.set_xlim(0,157)
ax.set_xticks([0, 50, 100, 150])
f.tight_layout()
#f.savefig('png/trajectories_y-s_allParticles_singleParticle.png', dpi=300)
#f.savefig('png/trajectories_y-s_allParticles.png', dpi=300)
#f.savefig('png/trajectories_y-s_singleParticle.png', dpi=300)


# %%
#----------------------------------------------------------------------
# Phase trajectories of particles in y-x-s
#----------------------------------------------------------------------
f = plt.figure(figsize=(13,13), facecolor='white')
ax = f.add_subplot(111, projection='3d')
fontsize=20
ax.set_xlabel('$s$ (m)', fontsize=fontsize)
ax.xaxis.labelpad = 40
ax.set_ylabel('$x$ (mm)', fontsize=fontsize)
ax.yaxis.labelpad = 20
ax.set_zlabel('$y$ (mm)', fontsize=fontsize)
ax.tick_params(labelsize=fontsize)
# Containts the injection bump...
#ax.plot(s0[single_particle_id,:], x0[single_particle_id,:]*1e3, y0[single_particle_id,:]*1e3,'-',ms=1.5,c='red', lw=5)
#for single_particle_id in range(n_part):
#    ax.plot(s0[single_particle_id,250:-180], x0[single_particle_id,250:-180]*1e3, y0[single_particle_id,250:-180]*1e3,'-',ms=1.,c='blue', lw=0.05)
single_particle_id = 0
ax.plot(s0[single_particle_id,250:-180], x0[single_particle_id,250:-180]*1e3, y0[single_particle_id,250:-180]*1e3,'-',ms=1.,c='red', lw=5)
ax.set_box_aspect([4, 1, 1])
ax.grid(False)
elevation_angle = 10
azimuthal_angle = -40
ax.view_init(elevation_angle, azimuthal_angle)
ax.set_facecolor('white')
ax.set_xlim(0,157)
ax.set_xticks([0, 50, 100, 150])
ax.set_zlim(-21,21)
ax.set_zticks([-20, 0, 20])
ax.set_ylim(-21,21)
ax.set_yticks([-20, 0, 20])
ax.grid(True)
f.tight_layout()
f.savefig('png/trajectories_y-s-x_singleParticle.png', dpi=300)
#f.savefig('png/trajectories_y-s-x_allParticles.png', dpi=300)
#f.savefig('png/trajectories_y-s-x_allParticles_singleParticle.png', dpi=300)


# %%
#----------------------------------------------------------------------
# Bunch in y-s
#----------------------------------------------------------------------
f,ax = plt.subplots(1,1,figsize=(15,4), facecolor='white')
fontsize=20
ax.set_xlabel('$s+z$ (m)', fontsize=fontsize)
ax.set_ylabel('$y$ (mm)', fontsize=fontsize)
ax.tick_params(labelsize=fontsize)
# PSB periodicity
for i in range(17):
    ax.axvline(157.08/16*i, ls='--', lw=1, c='grey')
# All particles
location = 3000
ax.plot((s0[0,location]+z0[:,location])%157.08, y0[:,location]*1e3,'.',ms=5,c='blue', lw=0.5)
single_particle_id = 0
ax.plot((s0[0,location]+z0[single_particle_id,location])%157.08, y0[single_particle_id,location]*1e3,'*',ms=25,c='red', lw=0.5)
ax.set_ylim(-21,21)
ax.set_yticks([-20, 0, 20])
ax.set_xlim(0,157)
ax.set_xticks([0, 50, 100, 150])
f.tight_layout()
#f.savefig(f'png/bunch_y-s_allParticles_singleParticle_ats{location}.png', dpi=300)


# %%
#----------------------------------------------------------------------
# Bunch in x-y-s
#----------------------------------------------------------------------
f = plt.figure(figsize=(13,13), facecolor='white')
ax = f.add_subplot(111, projection='3d')
fontsize=20
ax.set_xlabel('$s+z$ (m)', fontsize=fontsize)
ax.xaxis.labelpad = 40
ax.set_ylabel('$x$ (mm)', fontsize=fontsize)
ax.yaxis.labelpad = 20
ax.set_zlabel('$y$ (mm)', fontsize=fontsize)
ax.tick_params(labelsize=fontsize)
location = 3000
ax.plot((s0[0,location]+z0[:,location])%157.08, x0[:,location]*1e3, y0[:,location]*1e3,'.',ms=5,c='blue', lw=1)
single_particle_id = 0
ax.plot((s0[0,location]+z0[single_particle_id,location])%157.08, x0[single_particle_id,location]*1e3, y0[single_particle_id,location]*1e3,'*',ms=25,c='red', lw=1)
ax.set_box_aspect([4, 1, 1])
ax.grid(False)
elevation_angle = 10
azimuthal_angle = -40
ax.view_init(elevation_angle, azimuthal_angle)
ax.set_xlim(0,157)
ax.set_xticks([0, 50, 100, 150])
ax.set_zlim(-21,21)
ax.set_zticks([-20, 0, 20])
ax.set_ylim(-21,21)
ax.set_yticks([-20, 0, 20])
ax.set_facecolor('white')
ax.grid(True)
f.tight_layout()
#f.savefig(f'png/bunch_y-s-x_allParticles_singleParticle_ats{location}.png', dpi=300)

# %%
