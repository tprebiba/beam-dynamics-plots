import optics.toy_lattice as otl
import numpy as np
import matplotlib.pyplot as plt
import time
import NAFFlib


# Lattice
Qyset = 4.551
optics_ring = {'alfa_x':0.0,'beta_x':1.0,'mu_x':4.17, 'd_x':0.0, 'dp_x':0,
               'alfa_y':0.0,'beta_y':1.0,'mu_y':Qyset, 'd_y':0.0, 'dp_y':0}
map = otl.one_turn_map(**optics_ring)

# Beam parameters
n_part = 50
# 0: x, 1: px, 2: y, 3: py, 4: dpp
p = otl.particle_line(n_part, xmax=0.0, ymax=0.020, pxmax=0.0, pymax=0.020/3)

# Perturbations
L = 1.0
k1 = -6.15363e-4*4 # quadrupolar error
k2 = 50*0 # sextupolar error

# Track
n_turns = 2000
y = np.zeros((n_part,n_turns))
py = np.zeros((n_part,n_turns))
print('Starting tracking....')
st = time.time()
# Include_space charge
n_sckicks = 89
if n_sckicks>0:
	optics_ring['mu_y'] = optics_ring['mu_y']/n_sckicks
	optics_ring['mu_x'] = optics_ring['mu_x']/n_sckicks
	map = otl.one_turn_map(**optics_ring)
	Nb = 80e10
	sigma = 0.005 # beam size in m
p0 = p
y[:,0] = p0[2]
py[:,0] = p0[3]
for i in range(1,n_turns):
	p0[2] = y[:,i-1]
	p0[3] = py[:,i-1]
	if n_sckicks>0:
		for j in range(n_sckicks):
			p1 = map.track(p0)
			p1[3] = p1[3] - otl.sckick(p1[2],sigma,Nb)/n_sckicks
			p0,p1 = p1,p0
	else:
		p1 = map.track(p0)
		p0,p1 = p1,p0

	y[:,i] = np.copy(p0[2])
	py[:,i] = np.copy(p0[3]- k1*L*p1[2] - 0.5*k2*L*p1[2]**2.0)
	
et = time.time()
print('Tracked %i particles for %i turns in %1.5f seconds.'%(n_part,n_turns,et-st))

# Tunes
nn = 140
#qx = NAFFlib.multiparticle_tunes(y[:,0:nn]-1.j*py[:,0:nn],order=2,interpolation=0)
qy = NAFFlib.multiparticle_tunes(y[:,0:nn],order=4,interpolation=0)


#%%
# Plots
fig, axs = plt.subplots(1,2,figsize=(12,5), facecolor='w')
fontsize=24
xlim = 12
ms = 0.5

# Phase space
ax = axs[0]
ax.set_xlabel('$y$ [mm]', fontsize=fontsize) 
ax.set_ylabel('$p_y$ [mrad]', fontsize=fontsize)
ax.tick_params(labelsize=fontsize)
ax.set_ylim(-xlim,xlim)
ax.set_xlim(-xlim,xlim)
ax.grid(True)
for i in range(n_part):
    ax.plot(y[i,:]*1e3, py[i,:]*1e3, '.', ms=ms, c='blue')

# Overall tune
ax = axs[1]
ax.set_xlabel('$J_y$ [10$^{-5}$]', fontsize=fontsize) 
ax.set_ylabel('$Q_y$', fontsize=fontsize)
ax.tick_params(labelsize=fontsize-4)
ax.axhline(Qyset,c='grey',ls='--',lw=2.0)
ax.text(0,Qyset-0.005,'Set tune',c='grey',fontsize=fontsize-10)
if abs(k1)>0: clr = 'red'
else: clr='black'
ax.axhline(4.5, ls='-', color=clr, lw=2.0, label='Half-integer')
A = 0.5*(y**2+py**2)*1e5
qyreal = np.copy(qy)
if Qyset>4.5:
    qyreal[np.argmax(qyreal)::] = 1-qyreal[np.argmax(qyreal)::] # invert tunes "manually"
    pass
ax.plot(A[:,0], qyreal+4, '.', color='blue') 
ax.set_ylim(4.465, 4.555)

fig.tight_layout()
plt.show()
