import optics.toy_lattice as otl
import numpy as np
import matplotlib.pyplot as plt
import time


# Lattice
optics_ring = {'alfa_x':-0.14684*0,'beta_x':5.8096,'mu_x':4.17, 'd_x':1.4687, 'dp_x':0,
               'alfa_y':-0.19932*0,'beta_y':4.3730,'mu_y':4.23, 'd_y':0.0000, 'dp_y':0}
map = otl.one_turn_map(**optics_ring)


# Beam parameters
beta_gamma = 0.519753*1.170526
emit = 0.4535e-6 # normalized
n_part = 150
beam_params = {'eps_x': emit/beta_gamma, 'eps_y': emit/beta_gamma,
               'dpp_rms': 1.85E-3, 'n_part': n_part, 'xcenter': 0, 'ycenter': 0}
# Matched Gaussian
# 0: x, 1: px, 2: y, 3: py, 4: dpp
p = otl.generate_beam(optics_ring, beam_params)
#p = otl.particle_line(n_part)

# Track
n_turns = 3000
y = np.zeros((n_part,n_turns))
py = np.zeros((n_part,n_turns))
print('Starting tracking....')
st = time.time()
for i in range(n_turns):
	p = map.track(p)
	y[:,i] = p[2]
	py[:,i] = p[3]
et = time.time()
print('Tracked %i particles for %i turns in %1.5f seconds.'%(n_part,n_turns,et-st))
plt.plot(y*1e3,py*1e3, '.', ms=1.0, color='blue')
plt.show()
plt.tight_layout()
plt.savefig('linear_phase_space.png',dpi=400)
