import xpart as xp
import xobjects as xo
import xfields as xf
import xtrack as xt

import numpy as np
import matplotlib.pyplot as plt
from cpymad.madx import Madx


bumper_names = ['bi1.bsw1l1.1', 'bi1.bsw1l1.2', 'bi1.bsw1l1.3', 'bi1.bsw1l1.4']
thick_bumpers = {
'bi1.bsw1l1.1' : {'k0_name': 'k0BI1BSW1L11'},
'bi1.bsw1l1.2' : {'k0_name': 'k0BI1BSW1L12'},
'bi1.bsw1l1.3' : {'k0_name': 'k0BI1BSW1L13'},
'bi1.bsw1l1.4' : {'k0_name': 'k0BI1BSW1L14'},
}
vars = {
    'bumper_shifts': 1, 
    'on_bumper_edges': 1,
}

###############################################
# Get thick PSB lattice in MAD-X
###############################################
madthick = Madx()
madthick.globals.thin = 0
for nn in vars:
    madthick.globals[nn] = vars[nn]
madthick.call('psb_flat_bottom.madx')
seqthick = madthick.sequence.psb1
madthick.input('twiss, centre=True;')
twthick = madthick.table.twiss.dframe()
svthick = madthick.survey()

# Twiss with PTC
madthick.call('./psb/injection_chicane/ptc_twiss.madx')
madthick.input('exec, ptc_twiss_macro')
twthick_ptx = madthick.table.ptc_twiss.dframe()

###############################################
# Get thin PSB lattice in MAD-X
###############################################
madthin = Madx()
madthin.globals.thin = 1
for nn in vars:
    madthin.globals[nn] = vars[nn]
madthin.call('psb_flat_bottom.madx')
seqthin = madthin.sequence.psb1

# We modify the edge element on the thin lattice to fix issue with makethin
for ii, nn in enumerate(bumper_names):
    ll = madthick.sequence.psb1.expanded_elements[nn].l
    k0_name = thick_bumpers[nn]['k0_name']
    madthin.input(
      f"{nn}_den, e1 := {k0_name}*{ll}/2 * on_bumper_edges, h := {k0_name} * on_bumper_edges ;"
      f"{nn}_dex, e1 := {k0_name}*{ll}/2 * on_bumper_edges, h := {k0_name} * on_bumper_edges ;"
    )

# We have to set the angle ot the multipoles to a small number to avoid
# MAD-X assumption angle=knl[0] which is used when angle is 0
ee_thin_names = seqthin.element_names()
for nn in thick_bumpers.keys():
    for ee_nn in ee_thin_names:
        if ee_nn.startswith(nn+'..'):
           seqthin.expanded_elements[ee_nn].angle = 1e-20

madthin.input('twiss;')
twthin = madthin.table.twiss.dframe()
svthin = madthin.survey()
#for mad in [madthick, madthin]:
#    mad.input('twiss, betx=1, bety=1, x=0')


###############################################
# Convert to Xtrack line
###############################################
line = xt.Line.from_madx_sequence(seqthin, 
                                  apply_madx_errors=True,
                                  deferred_expressions=True)
line.particle_ref = xp.Particles(mass0=xp.PROTON_MASS_EV, q0=1.0, gamma0=seqthin.beam.gamma)
line.build_tracker()

# Remove horizontal component of edges
assert np.abs(line.vars['bsw_k0l']._value > 0.01)
bsw_k0l_ref = line.vars['bsw_k0l']._value
for nn in bumper_names:
    for suffix in ['_den', '_dex']:
        r43 = line[nn+suffix].r43
        line[nn+suffix].r21 = 0

        line.element_refs[nn+suffix].r43 = (
            r43 * line.vars['bsw_k0l'] / bsw_k0l_ref * line.vars['on_bumper_edges'])
tw = line.twiss()
line.to_json('psb/psb_line.json')


###############################################
# Make some plots
###############################################
plt.close('all')
plt.figure(1, figsize=(6.4*2, 4.8))
sp1x = plt.subplot(221)
plt.plot(twthick['s'],twthick['betx'], label='mad thick')
plt.plot(twthick_ptx['s'],twthick_ptx['betx'], label='thick ptc')
plt.plot(tw.s, tw.betx, label='xsuite')
plt.ylabel(r'$\beta_x$ [m]')
plt.xlabel('s [m]')

sp1y = plt.subplot(222, sharex=sp1x)
plt.plot(twthick['s'],twthick['bety'], label='mad thick')
plt.plot(twthick_ptx['s'],twthick_ptx['bety'], label='thick ptc')
plt.plot(tw.s, tw.bety, label='xsuite')
plt.ylabel(r'$\beta_y$ [m]')
plt.xlabel('s [m]')

sp2x = plt.subplot(223, sharex=sp1x)
plt.plot(twthick['s'],twthick['x'], label='mad thick')
plt.plot(twthick_ptx['s'],twthick_ptx['x'], label='thick ptc')
plt.plot(tw.s, tw.x, label='xsuite')
plt.ylabel(r'$x$ [m]')
plt.xlabel('s [m]')

sp2y = plt.subplot(224, sharex=sp1x)
plt.plot(twthick['s'],twthick['y'], label='mad thick')
plt.plot(twthick_ptx['s'],twthick_ptx['y'], label='thick ptc')
plt.plot(tw.s, tw.y, label='xsuite')
plt.ylabel(r'$y$ [m]')
plt.xlabel('s [m]')


plt.legend(loc='best')

plt.figure(2)
plt.plot(svthick.s, svthick.x)
plt.plot(svthin.s, svthin.x)
plt.show()