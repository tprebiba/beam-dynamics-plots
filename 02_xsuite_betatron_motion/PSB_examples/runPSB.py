#%%
import xtrack as xt
import xpart as xp
import xobjects as xo
import xfields as xf

import numpy as np
import matplotlib.pyplot as plt
from cpymad.madx import Madx

import optics.psb_lattice as psb

import tfs
import json


#%%
####################
# Choose a context #
####################
context = xo.ContextCpu()
# context = xo.ContextCupy()
#context = xo.ContextPyopencl('0.0')
print(context)


#%%
#############################
# Load PSB line
#############################
line = xt.Line.from_json('psb/psb_line.json')
line.unfreeze()
#line.cycle(index_first_element=1050, inplace=True) # br.bhz81 at s = 72.185; for plotting convenience of injection chicane
line.build_tracker(_context=context)


#%%
#############################
# Unassign errors
#############################
line.vars['bsw_k0l'] = 0.0
line.vars['bsw_k2l'] = 0.0
bety_ref = line.twiss()['bety', 'mymarker']
alfy_ref = line.twiss()['alfy', 'mymarker']


#%%
#############################
# Match tunes and Twiss it
#############################
qx_target = 4.40
qy_target = 4.45
line.match(
              vary=[
                    xt.Vary('kbrqf', step=1e-8),
                    xt.Vary('kbrqd', step=1e-8),
              ],
              targets = [
                         xt.Target('qx', qx_target, tol=1e-5),
                         xt.Target('qy', qy_target, tol=1e-5)
              ]
)
twbefore = line.twiss().to_pandas()
#twbefore.to_pickle(r'outputs/twiss_withoutErrors_Qx%sQy%s.pkl'%(qx_target, qy_target))


#%%
#############################
# Assign errors
#############################
error = 'dipolar' # 'none' or 'injection_chicane' or 'dipolar' or 'quadrupolar'
if error == 'injection_chicane': 
      bswdf = tfs.read("psb/injection_chicane/BSW_collapse.tfs")
      assign_chicane_tablerow = 0
      line.vars['bsw_k0l'] = bswdf.BSW_K0L[0]
      line.vars['bsw_k2l'] = bswdf.BSW_K2L[0]
      twafter = line.twiss().to_pandas()
      twafter.to_pickle(r'outputs/twiss_withInjection_Qx%sQy%s.pkl'%(qx_target, qy_target))
      rematchit = False
      if rematchit:
            line.match(verbose=True,
              vary=[
                    xt.Vary('kbrqd3corr', step=1e-5, limits=[-0.01, 0.0]),
                    xt.Vary('kbrqd14corr', step=1e-5, limits=[-0.01, 0.0]),
              ],
              targets = [
                         xt.Target('bety', at='mymarker', value=bety_ref, tol=1e-4, scale=1),
                         xt.Target('alfy', at='mymarker', value=alfy_ref, tol=1e-4, scale=1)
                         
              ]
            )
            twcorrected = line.twiss().to_pandas()
            twcorrected.to_pickle(r'outputs/twiss_correctedInjection_Qx%sQy%s.pkl'%(qx_target, qy_target))
            print(line.vars['kbrqd3corr']._value)
            print(line.vars['kbrqd14corr']._value)
elif error == 'dipolar':
      line.vars['kbr1dvt2l4'] = 0.001 # just a dipole corrector magnet
      #line.vars['kbr1dvt10l4'] = 10
elif error == 'quadrupolar':
      pass



#%%
######################
# Generate particles #
######################
n_part = int(1.2e3)
bunch_intensity = 40e10
emitx = 1.0e-6
emity = 1.0e-6
sigma_z = (271/4)*0.525*0.3
particles = xp.generate_matched_gaussian_bunch(num_particles=int(n_part), total_intensity_particles=bunch_intensity, 
                                                nemitt_x=emitx, nemitt_y=emity, sigma_z=sigma_z,
                                                line=line)
# "Force" coasting beam
particles.zeta = np.random.uniform(-1e-2, 1e2, n_part)
particles.delta = np.random.uniform(-1e-4, 1e-4, n_part)
with open(f'outputs/particles_ini.json', 'w') as fid:
      json.dump(particles.to_dict(), fid, cls=xo.JEncoder)


#%%
#############################
# Start tracking
#############################
num_turns = 1
      
# track one turn
line.track(particles,
           num_turns=num_turns, 
           turn_by_turn_monitor='ONE_TURN_EBE' # to save in all elements
           #turn_by_turn_monitor=True
           )
with open(f'outputs/particles_fin.json', 'w') as fid:
      json.dump(particles.to_dict(), fid, cls=xo.JEncoder)

np.save('outputs/s', line.record_last_track.s[0])
#np.save('outputs/x',line.record_last_track.x)
#np.save('outputs/px',line.record_last_track.px)
np.save('outputs/y',line.record_last_track.y)
np.save('outputs/py',line.record_last_track.py)
#np.save('outputs/z',line.record_last_track.zeta)
#np.save('outputs/d',line.record_last_track.delta)
# %%
