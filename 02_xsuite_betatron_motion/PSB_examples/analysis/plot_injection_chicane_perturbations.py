#%%
import numpy as np  
import matplotlib.pyplot as plt
import pandas as pd
twbefore = pd.read_pickle('../outputs/twiss_withoutErrors_Qx4.4Qy4.46.pkl')
twafter = pd.read_pickle('../outputs/twiss_withInjection_Qx4.4Qy4.46.pkl')
twcorrected = pd.read_pickle('../outputs/twiss_correctedInjection_Qx4.4Qy4.46.pkl')


#%%
#############################
# Plot optics
#############################
f,ax = plt.subplots(1,1,figsize=(15,4.5), facecolor='white')
fontsize=20
ax.set_ylabel(r'$\beta_y$ (m)', fontsize=fontsize)
ax.set_xlabel(r'$s-s_{injection}$ (m)', fontsize=fontsize)
ax.tick_params(labelsize=fontsize)
#ax.plot(twbefore.s-(157.08-72.185+1.5), twbefore.bety, label='unperturbed',lw=2,c='blue')
ax.plot(twafter.s-(157.08-72.185+1.5), twafter.bety, label='perturbed',lw=2,c='blue')
#ax.plot(twafter.s-(157.08-72.185+1.5), twcorrected.bety, label='perturbed',lw=2,c='blue')
ax.axvline(-1.5, c='red', ls='--', lw=1.5)
ax.axvline(1.5, c='red', ls='--', lw=1.5)
ax.text(-0.8, 15, 'Injection perturbations', rotation=90, c='red')
ax.axvline(25.87-1.5, c='green', ls='--', lw=1.5)
ax.text(25.87-1.5+0.3, 20, 'QDE3 corrector', rotation=90, c='green')
ax.axvline(-25.87+1.3, c='green', ls='--', lw=1.5)
ax.text(-25.87+1.3+0.3, 20, 'QDE14 corrector', rotation=90, c='green')
ax.set_xlim(-70,70)
ax.set_ylim(0,38)
f.tight_layout()
#f.savefig('plots/unperturbed_lattice.png', dpi=400)
#f.savefig('plots/perturbed_lattice.png', dpi=400)
#f.savefig('plots/perturbed_lattice_correctors.png', dpi=400)
#f.savefig('plots/corrected_lattice_correctors.png', dpi=400)
# %%
