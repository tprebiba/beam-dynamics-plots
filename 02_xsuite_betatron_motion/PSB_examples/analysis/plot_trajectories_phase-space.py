#%%
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import optics.psb_lattice as psb
from cpymad.madx import Madx
import matplotlib.patches as patches

def plotLatticeSeries(ax,series, height=1., v_offset=0., color='r',alpha=0.5,label=''):
    aux=series
    ax.add_patch(
    patches.Rectangle(
        (aux.s-aux.l, v_offset-height/2.),   # (x,y)
        aux.l,          # width
        height,         # height
        color=color, alpha=alpha, label=label))
    return;


# %%
#----------------------------------------------------------------------
# Get data
#----------------------------------------------------------------------
s = np.load('../outputs/s.npy')
y0 = np.load('../outputs/y0.npy')
py0 = np.load('../outputs/py0.npy')
y1 = np.load('../outputs/y1.npy')
y2 = np.load('../outputs/y2.npy')
twiss = pd.read_pickle('../outputs/twiss_withoutErrors_Qx4.4Qy4.45.pkl') # for envelope
twiss2 = pd.read_pickle('../outputs/twiss_withInjection_Qx4.4Qy4.465.pkl') # for envelope
print('particles', len(y0))
print('elements', len(y0[0]))

#madthin = Madx()
#twtablethin = psb.get_MADX_lattice(madthin, QH_target = 4.17, QV_target=4.23, makethin=True, slice=3)
madthick = Madx()
twtablethick = psb.get_MADX_lattice(madthick, QH_target = 4.40, QV_target=4.45, load='eos',year='2021')
myTwiss=madthick.table.twiss.dframe()
psb.assign_chicane_perturbations(QH_target = 4.40, QV_target=4.45, mad=madthick,assign_chicane_tablerow=2,use_BSW_alignment=1)
twtablethick=psb.get_twiss_table(madthick,centre=True)


#%% 
#----------------------------------------------------------------------
# Plot elements around the ring
#----------------------------------------------------------------------
fig,ax1 = plt.subplots(1,1,figsize=(12,3), facecolor='white')
fontsize=15
#plt.title('CERN Proton Synchrotron Booster',fontsize=fontsize)
ax1.axhline(0, ls='-', lw=1, c='black')
for i in range(16):
    ax1.axvline(157.06/16*i, ls='--', lw=1, c='grey')
    ax1.text(157.08/16*i+157.08/16/4, -0.8, str(i+1), c='red', fontsize=fontsize)
ax1.axvline(157.08, ls='--', lw=1, c='grey')
DF=myTwiss[(myTwiss['keyword']=='quadrupole')]
ax1.set_xlabel('s (m)', fontsize=fontsize)
for i in range(len(DF)):
    lbl=''
    aux=DF.iloc[i]
    if aux.k1l>0: 
        if i==0: lbl='Focusing Quadrupoles'
        plotLatticeSeries(plt.gca(),aux, height=aux.k1l, v_offset=aux.k1l/2, color='green',label=lbl)
    elif aux.k1l<0: 
        if i==1: lbl='Defocusing Quadrupoles'
        plotLatticeSeries(plt.gca(),aux, height=aux.k1l, v_offset=aux.k1l/2, color='blue',label=lbl)
ax1.tick_params(labelleft=False, left=False, labelsize=fontsize)
ax1.set_ylim(-.9,.9)
ax1.set_xlim(0,157)
ax2 = ax1.twinx()
ax2.tick_params(axis='y', labelright=False, right=False)
ax2.set_ylim(-.8,.8)
DF=myTwiss[(myTwiss['keyword']=='sbend')]
for i in range(len(DF)):
    aux=DF.iloc[i]
    lbl='' 
    if i==0: 
        lbl='Dipoles'
    plotLatticeSeries(ax2, aux, height=aux.angle, v_offset=aux.angle/2*0, color='red', label=lbl)
ax1.legend(loc=1, fontsize=fontsize-2)
ax2.legend(loc=2, fontsize=fontsize-2)
fig.tight_layout()
#fig.savefig('plots/elements_around_the_ring_nothing.png', dpi=400)
#fig.savefig('plots/elements_around_the_ring_dipoles.png', dpi=400)
#fig.savefig('plots/elements_around_the_ring_dipoles_focusing_quads.png', dpi=400)
#fig.savefig('plots/elements_around_the_ring.png', dpi=400)


# %%
#----------------------------------------------------------------------
# Phase trajectories of particles
#----------------------------------------------------------------------
f,ax = plt.subplots(1,1,figsize=(15,4), facecolor='white')
fontsize=20
ax.set_xlabel('$s$ (m)', fontsize=fontsize)
ax.set_ylabel('$y$ (mm)', fontsize=fontsize)
ax.set_ylim(-20,20)
ax.set_xlim(0,157)
ax.tick_params(labelsize=fontsize)
for i in range(16):
    ax.axvline(157.06/16*i, ls='--', lw=1, c='grey')
ax.axvline(157.08, ls='--', lw=1, c='grey')
for i in range(0,1200):
    ax.plot(s, y0[i,:]*1e3,'-',ms=1.5,c='black', lw=0.1)
    pass
ax.plot(s, y0[i,:]*1e3,'-',ms=1.5,c='black', lw=1.5, label='Beam particles trajectories')
single_particle_id = 1
#ax.plot(s, y0[single_particle_id, :]*1e3, '-', c='red', lw=3, label='Single particle trajectory')
ax.plot(s, np.mean(y0, axis=0)*1e3, '-', c='red', lw=3, label='Beam centroid trajectory')
#nsigmas = 3
#ax.plot(twiss.s, np.sqrt(twiss.bety*1e3*1.0*1e-3)*nsigmas, '-', c='blue', lw=5.0, label=r'Beam envelope (3$\sigma$)')
#ax.plot(twiss.s, -np.sqrt(twiss.bety*1e3*1.0*1e-3)*nsigmas, '-', c='blue', lw=5.0)
ax.legend(loc=1, fontsize=fontsize-2)
f.tight_layout()
#f.savefig('plots/single_particle_trajectory.png', dpi=400)
#f.savefig('plots/single_particle_and_bunch_trajectory.png', dpi=400)
#f.savefig('plots/bunch_trajectory.png', dpi=400)
#f.savefig('plots/bunch_trajectory_and_beam_centroid_trajectory.png', dpi=400)
#f.savefig('plots/single_particle_and_bunch_trajectory_and_3sigma_envelope.png', dpi=400)
#f.savefig('plots/bunch_trajectory_and_3sigma_envelope.png', dpi=400)
#plt.show()


#%%
#----------------------------------------------------------------------
# Optics around the ring
#----------------------------------------------------------------------
f,ax = plt.subplots(1,1,figsize=(15,3.5), facecolor='white')
fontsize=20
ax.set_xlabel('$s$ (m)', fontsize=fontsize)
ax.set_ylabel('(m)', fontsize=fontsize)
ax.set_ylim(0,32)
ax.set_xlim(0,157)
for i in range(16):
    ax.axvline(157.06/16*i, ls='--', lw=1, c='grey')
ax.axvline(157.08, ls='--', lw=1, c='grey')
ax.plot(twiss['s'],twiss['bety'],c='blue', label=r'$\beta_y$')
ax.plot(myTwiss['s'],myTwiss['bety'],c='blue', label=r'$\beta_y$',lw=2)
ax.plot(twtablethick['s'],twtablethick['bety'],c='red', label=r'$\beta_y$ (distorted)',lw=2)
ax.tick_params(labelsize=fontsize)
ax.legend(loc=0)
f.tight_layout()
#f.savefig('plots/optics_ring.png', dpi=400)
#f.savefig('plots/optics_ring_bety.png', dpi=400)
#f.savefig('plots/optics_ring_bety_distored.png', dpi=400)



#%% 
#----------------------------------------------------------------------
# Phase space y-py for different s-positions over 1 turn
#----------------------------------------------------------------------
indx=2002 # 590 1250 2002
f,ax = plt.subplots(1,1,figsize=(6,5), facecolor='white')
fontsize=15
ax.set_title('At $s=%1.1f$ m'%s[indx], fontsize=fontsize)
ax.set_xlabel('$y$ (mm)', fontsize=fontsize)
ax.set_ylabel('$p_y$ (mrad)', fontsize=fontsize)
ax.tick_params(labelsize=fontsize)
ax.plot(y0[:,indx]*1e3, py0[:,indx]*1e3, '.', ms=8, c='black')
ax.set_xlim(-13,13)
ax.set_ylim(-5,5)
f.tight_layout()
#f.savefig('plots/phase_space_%05d.png'%indx, dpi=100)
#plt.close()
plt.show()


# %%
#----------------------------------------------------------------------
# Trajectories with dipolar error
#----------------------------------------------------------------------
f,ax = plt.subplots(1,1,figsize=(15,4), facecolor='white')
fontsize=20
ax.set_xlabel('$s$ (m)', fontsize=fontsize)
ax.set_ylabel('$y$ (mm)', fontsize=fontsize)
ax.set_ylim(-20,20)
ax.set_xlim(0,157)
ax.tick_params(labelsize=fontsize)
for i in range(16):
    ax.axvline(157.06/16*i, ls='--', lw=1, c='grey')
ax.axvline(157.08, ls='--', lw=1, c='grey')
for i in range(0,1200):
    ax.plot(s, y2[i,:]*1e3,'-',ms=1.5,c='black', lw=0.1)
    pass
ax.plot(s, y2[i,:]*1e3,'-',ms=1.5,c='black', lw=1.5, label='Beam particles trajectories')
ax.plot(s, np.mean(y2, axis=0)*1e3, '-', c='red', lw=3, label='Beam centroid trajectory')
ax.legend(loc=1, fontsize=fontsize-2)
f.tight_layout()
#f.savefig('plots/bunch_trajectories_withDipolarError.png', dpi=400)
#plt.show()


# %%
#----------------------------------------------------------------------
# Trajectories with beta-beating
#----------------------------------------------------------------------
f,ax = plt.subplots(1,1,figsize=(15,4), facecolor='white')
fontsize=20
ax.set_xlabel('$s$ (m)', fontsize=fontsize)
ax.set_ylabel('$y$ (mm)', fontsize=fontsize)
ax.set_ylim(-20,20)
ax.set_xlim(0,157)
ax.tick_params(labelsize=fontsize)
for i in range(16):
    ax.axvline(157.06/16*i, ls='--', lw=1, c='grey')
ax.axvline(157.08, ls='--', lw=1, c='grey')
for i in range(0,1200):
    ax.plot(s, y1[i,:]*1e3,'-',ms=1.5,c='black', lw=0.1)
    pass
ax.plot(s, y1[i,:]*1e3,'-',ms=1.5,c='black', lw=1.5, label='Beam particles trajectories')
single_particle_id = 1
#ax.plot(s, y0[single_particle_id, :]*1e3, '-', c='red', lw=3, label='Single particle trajectory')
nsigmas = 3
#ax.plot(twiss.s, np.sqrt(twiss.bety*1e3*1.0*1e-3)*nsigmas, '-', c='blue', lw=5.0, label=r'Beam envelope (3$\sigma$)')
ax.plot(twiss2.s, np.sqrt(twiss2.bety*1e3*1.0*1e-3)*nsigmas, '-', c='blue', lw=5.0, label=r'Beam envelope (3$\sigma$)')
ax.plot(twiss2.s, -np.sqrt(twiss2.bety*1e3*1.0*1e-3)*nsigmas, '-', c='blue', lw=5.0)
ax.legend(loc=1, fontsize=fontsize-2)
f.tight_layout()
#f.savefig('plots/bunch_trajectories_withBetaBeating.png', dpi=400)
#plt.show()

# %%
