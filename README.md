# Beam Dynamics Plots

A set of simple simulations to create instructive plots for transverse beam dynamics lectures. 

Use with: 

beam-analysis-sandbox/beamtools: pip install git+https://gitlab.cern.ch/tprebiba/beam-analysis-sandbox.git#subdirectory=beamtools

beam analysis-sandbox/optics: pip install git+https://gitlab.cern.ch/tprebiba/beam-analysis-sandbox.git#subdirectory=optics

xsuite: pip install xsuite

PySCRDT: pip install git+https://github.com/fasvesta/PySCRDT.git